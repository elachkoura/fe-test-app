import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from 'src/services/users/users.service';

@Component({
  selector: 'app-add-user-page',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit
{
  public form: FormGroup;
  public genericError: string = null;
  public isExist = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private usersService: UsersService
  ) { }

  ngOnInit(): void
  {
    this.form = this.fb.group({
      username: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(20), Validators.pattern('^[A-Za-z0-9]+$')]],
      email: [null, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      first_name: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
      id_status: [1]
    });
  }

  /**
   * Go back
   */
  public goBack()
  {
    this.router.navigateByUrl('/users');
  }

  /**
   * Add user
   */
  public addUser(data: FormGroup)
  {
    this.isExist = false;

    this.usersService
      .postUser(data)
      .then(result =>
      {
        if (result)
        {
          this.router.navigateByUrl('/users');
        }
      }).catch(error =>
      {
        this.isExist = true;
        this.form.controls['username'].setErrors({ 'isExist': true });
        this.genericError = error;
        // Handle error gently
      });
  }
}
