import { Component, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, Sort } from '@angular/material/sort';
import { UsersService } from 'src/services/users/users.service';
import { StatusesService } from 'src/services/statuses/statuses.service';
import { User } from 'src/models/user';
import { Status } from 'src/models/status';

@Component({
  selector: 'app-users-list-page',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent
{
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public statusesList: Array<Status> = [];
  public genericError: string = null;
  displayedColumns: string[] = [
    'username',
    'full_name',
    'email',
    'status',
    'created_date'
  ];
  dataSource = null;

  constructor(
    private usersService: UsersService,
    private translateService: TranslateService,
    private statusesService: StatusesService,
    private _liveAnnouncer: LiveAnnouncer,
    private router: Router
  )
  {
    this.getUsersList();
  }

  /**
   * Get List of users
   */
  public getUsersList()
  {
    this.statusesService
      .getStatuses()
      .then(statuses =>
      {
        this.statusesList = statuses;
      }).catch(error =>
      {
        this.genericError = error;
        // Handle error gently
      });

    this.usersService
      .getUsers()
      .then(data =>
      {
        data.map(user =>
        {
          this.statusesList.map(status =>
          {
            if (user.id_status === status.id)
            {
              user.tag_status = this.translateService.instant(status.tag);
            }
          });
        });
        this.dataSource = new MatTableDataSource<User>(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }).catch(error =>
      {
        this.genericError = error;
        // Handle error gently
      });
  }

  /**
   * Sort users
   */
  public announceSortChange(sortState: Sort)
  {
    if (sortState.direction)
    {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else
    {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  /**
   * Add new user
   */
  public addUser()
  {
    this.router.navigateByUrl('/add-user');
  }
}
