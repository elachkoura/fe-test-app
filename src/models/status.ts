export class Status
{
     private _id: number = null;
     private _tag: string = null;
     private _description: string = null;

     /**
      * Getter id
      * @return {number }
      */
     public get id(): number
     {
          return this._id;
     }

     /**
      * Getter tag
      * @return {string }
      */
     public get tag(): string
     {
          return this._tag;
     }

     /**
      * Getter description
      * @return {string }
      */
     public get description(): string
     {
          return this._description;
     }

     /**
      * Setter id
      * @param {number } value
      */
     public set id(value: number)
     {
          this._id = value;
     }

     /**
      * Setter tag
      * @param {string } value
      */
     public set tag(value: string)
     {
          this._tag = value;
     }

     /**
      * Setter description
      * @param {string } value
      */
     public set description(value: string)
     {
          this._description = value;
     }

}
