import { Status } from './status';

export class User
{
     private _id: number = null;
     private _first_name: string = null;
     private _last_name: string = null;
     private _email: string = null;
     private _created_date: string = null;
     private _id_status: number = null;
     private _tag_status: string = null;
     private _username: string = null;

     /**
      * Getter id
      * @return {number }
      */
     public get id(): number
     {
          return this._id;
     }

     /**
      * Getter first_name
      * @return {string }
      */
     public get first_name(): string
     {
          return this._first_name;
     }

     /**
      * Getter last_name
      * @return {string }
      */
     public get last_name(): string
     {
          return this._last_name;
     }

     /**
      * Getter email
      * @return {string }
      */
     public get email(): string
     {
          return this._email;
     }

     /**
      * Getter created_date
      * @return {string }
      */
     public get created_date(): string
     {
          return this._created_date;
     }

     /**
      * Getter id_status
      * @return {number }
      */
     public get id_status(): number
     {
          return this._id_status;
     }

     /**
      * Getter tag_status
      * @return {string }
      */
     public get tag_status(): string
     {
          return this._tag_status;
     }

     /**
      * Getter username
      * @return {string }
      */
     public get username(): string
     {
          return this._username;
     }

     /**
      * Setter id
      * @param {number } value
      */
     public set id(value: number)
     {
          this._id = value;
     }

     /**
      * Setter first_name
      * @param {string } value
      */
     public set first_name(value: string)
     {
          this._first_name = value;
     }

     /**
      * Setter last_name
      * @param {string } value
      */
     public set last_name(value: string)
     {
          this._last_name = value;
     }

     /**
      * Setter email
      * @param {string } value
      */
     public set email(value: string)
     {
          this._email = value;
     }

     /**
      * Setter created_date
      * @param {string } value
      */
     public set created_date(value: string)
     {
          this._created_date = value;
     }

     /**
      * Setter id_status
      * @param {number } value
      */
     public set id_status(value: number)
     {
          this._id_status = value;
     }

     /**
      * Setter tag_status
      * @param {string } value
      */
     public set tag_status(value: string)
     {
          this._tag_status = value;
     }

     /**
      * Setter username
      * @param {string } value
      */
     public set username(value: string)
     {
          this._username = value;
     }

}
