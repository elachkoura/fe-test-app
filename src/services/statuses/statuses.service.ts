import { Injectable } from '@angular/core';
import { NetworkService } from '../network/network.service';
import { MapperService } from '../mapper/mapper.service';

@Injectable({
  providedIn: 'root'
})
export class StatusesService
{

  constructor(
    private networkService: NetworkService,
    private mappersService: MapperService
  ) { }

  public getStatuses()
  {
    return this.networkService
      .get('statuses')
      .then(data =>
      {
        const statuses = this.mappersService._mapJsonToStatuses(data);
        return Promise.resolve(statuses);
      })
      .catch(error =>
      {
        return Promise.reject(error);
      });
  }
}
