import { Injectable } from '@angular/core';
import { NetworkService } from '../network/network.service';
import { MapperService } from '../mapper/mapper.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService
{

  constructor(
    private networkService: NetworkService,
    private mappersService: MapperService
  ) { }

  public getUsers()
  {
    return this.networkService
      .get('users')
      .then(data =>
      {
        const gamesList = this.mappersService._mapJsonToUsers(data);
        return Promise.resolve(gamesList);
      })
      .catch(error =>
      {
        return Promise.reject(error);
      });
  }

  public postUser(data)
  {
    const user = {
      'user': data.value
    };

    return this.networkService
      .post('users', user)
      .then(result =>
      {
        return Promise.resolve(result);
      })
      .catch(error =>
      {
        return Promise.reject(error);
      });
  }
}
