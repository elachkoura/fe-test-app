import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { User } from '../../models/user';
import { Status } from 'src/models/status';

@Injectable({
  providedIn: 'root'
})
export class MapperService
{
  constructor() { }

  // User Mapper
  public _mapJsonToUsers(arrayOfUser): Array<User>
  {
    return arrayOfUser.data.map(user => this._mapJsonToUser(user));
  }

  public _mapJsonToUser(userJson): User
  {
    const user = new User();

    user.id = userJson['id'];
    user.first_name = userJson['first_name'];
    user.last_name = userJson['last_name'];
    user.email = userJson['email'];
    user.id_status = userJson['id_status'];
    user.username = userJson['username'];
    user.created_date = userJson['created_date'] ? moment(userJson['created_date']).format('YYYY-MM-DD') : null;

    return user;
  }

  // Status Mapper
  public _mapJsonToStatuses(arrayOfStatus): Array<Status>
  {
    return arrayOfStatus.data.map(status => this._mapJsonToStatus(status));
  }

  public _mapJsonToStatus(statusJson): Status
  {
    const status = new Status();

    status.id = statusJson['id'];
    status.tag = statusJson['tag'];
    status.description = statusJson['description'];

    return status;
  }
}
